from abc import abstractmethod
import numpy as np

from scipy import ndimage as ndi

from skimage import data, img_as_float
from skimage.filters import rank, sobel
from skimage.morphology import disk
from skimage.segmentation import felzenszwalb,\
     morphological_geodesic_active_contour,slic,watershed 

from cenotaph.basics.base_classes import Image, ImageHandler, ImageType
from cenotaph.third_parties.doc_inherit import doc_inherit

class ImageSegmenter(ImageHandler):
    """Abstract base class for image segmentation"""
    
    @abstractmethod 
    def _segment(self, img):
        """Abstract method to be implemented by the subclasses.
        
        Parameters
        ----------
        img : Image
            The input image. 

        Returns
        ----------
        segmented : ndarray
            The segmented image. This is a greyscale image where each grey
            level corresponds to a different resgion. The size is the same as
            the input image.
        """
    
    @abstractmethod
    def _check_type(self, img):
        """Check the type of the input image is compatible with the segmenter. 
               
        Parameters
        ----------
        img : Image
            The image to segment
            
        Returns
        -------
        check_ok : bool
            True if img is the right type, false otherwise.
        """
    
    def get_result(self, img, **kwargs):
        """
        Get the segmentation result
        
        Parameters
        ----------
        img : Image
            The grey-scale input image to segment. 
        seed_pixel : int (2), optional
            Converts the result of the segmentation to a binary image where
            all the pixels that have the same value as seed_pixel are set
            to (2**bit_depth - 1), the others are set to zero.

        Returns
        ----------
        img_out : Image
            The segmented image. This is a greyscale image where each grey
            level corresponds to a different region.
        """
        
        #Check that the type of the input image is the correct one for the
        #segmentation method chosen
        if not self._check_type(img):
            raise Exception('The input image is not compatible with the'
                            'segmenter')
                
        #Call the class-specific segmenter and return the result
        data_out = self._segment(img).astype(np.uint)
        
        #Generate the output image
        img_out = Image.from_array(data = data_out, img_type = ImageType.GS)
        
        #Filter on the pivot pixel if required
        if 'seed_pixel' in kwargs.keys():
            img_out = self.filter_by_pixel(img_out, kwargs['seed_pixel'])
                
        return img_out
    
    @staticmethod
    def filter_by_pixel(img, seed_pixel):
        """Filter the result of the segmentation based on a given pixel.
        All the pixels with the same values as seed_pixel will be set to 1, all 
        the others to 0.
        
        Parameters
        ----------
        img : Image
            A greyscale image, usually is the result of the segmentation.
        pixel : int (2)
            The coordinates of the pixel.
            
        Returns
        -------
        img_out : Image
            A binary image.
        """
        
        #Compute and update the values
        new_data = img.get_data(copy = True)
        pivot_value = new_data[seed_pixel[0],seed_pixel[1]]       
        new_data[:] = 0
        new_data[np.where(img.get_data(copy = False) == pivot_value)] =\
            2**img.get_bit_depth() - 1
        
        #Create and return the output image
        img_out = Image.from_array(data = new_data, img_type = ImageType.BW)
        return img_out

class ImageSegmenterGS(ImageSegmenter):
    """Abstract base class for image segmentation of grey-scale images"""
    
    @doc_inherit
    def _check_type(self, img):
        """Return True if the image is greyscale, False otherwise"""
        
        check_ok = True
        if not (img.get_type() == ImageType.GS):
            check_ok = False  
        
        return check_ok
                
class MorphGAC(ImageSegmenterGS):
    """Image segmentation based on Morphological Geodesic Active Contours
    
    References
    ----------
    [1] Marquez-Neila, P., Baumela, L., Alvarez, L. A morphological approach 
        to curvature-based evolution of curves and surfaces (2014) 
        IEEE Transactions on Pattern Analysis and Machine Intelligence, 36 (1), 
        art. no. 6529072, pp. 2-17.
    """
    
    def __init__(self, n_iter=10, baloon_force=0.0, 
                 init_level_set = 'circle'):
        """
        Parameters
        ----------
        n_iter : int
            The number of iterations.
        baloon_force : float
            Balloon force to guide the contour in non-informative areas of the
            image where the gradient is too small to push the contour towards 
            a border. Negative values will shrink the contour, positive ones 
            will expand it. Setting this to zero disables the balloon force.
        init_level_set : str, Image
            A grey-scale or binary input image representing the initial level 
            set. Needs to have the same size as the image to segment. If a 
            string is given possible values are: 'checkerboard' and 'circle'.
        """
        self._n_iter = n_iter
        self._baloon_force = baloon_force
        self.set_init_level_set(init_level_set)
            
    def set_init_level_set(self, init_level_set):
        """
        Define the intitial level set for the segmentation
        
        Parameters
        ----------
        init_level_set : str, Image
            A grey-scale or binary input image representing the initial level 
            set. Needs to have the same size as the image to segment. If a 
            string is given possible values are: 'checkerboard' and 'circle'.
        """        
        
        #Check the input
        if isinstance(init_level_set, str):
            if init_level_set not in ['checkerboard', 'circle']:
                raise Exception('Initial level set of type str '
                                'not recognised')
            self._init_level_set = init_level_set
                
        elif isinstance(init_level_set, Image):
            if not ((init_level_set.get_type() == ImageType.GS) or\
               (init_level_set.get_type() == ImageType.BW)):
                raise Exception('Initial level set must be a greyscale '
                                'or binary image')
            self._init_level_set = init_level_set.get_data()
        else:
            raise Exception('Intial level set not recognised') 
        
            
    @doc_inherit   
    def _segment(self, img):
                            
        segmented = morphological_geodesic_active_contour(
            img_as_float(img.get_data()), iterations=self._n_iter, 
            init_level_set = self._init_level_set, 
            smoothing = 1, threshold='auto', 
            balloon = self._baloon_force)
        
        return segmented
    
    def __repr__(self):
        return self.__class__.__name__ +\
               ' num. iter. = {}, baloon force = {:4.2f}'.\
               format(self._n_iter, self._baloon_force)
        
class Watershed(ImageSegmenterGS):
    """Image segmentation based on watershed transform with compactness
    constraint.
    
    References
    ----------
    [1] https://en.wikipedia.org/wiki/Watershed_%28image_processing%29
    [2] http://cmm.ensmp.fr/~beucher/wtshed.html
    [3] Neubert, P., Protzel, P. Compact watershed and preemptive SLIC: On 
        improving trade-offs of superpixel segmentation algorithms (2014) 
        Proceedings - International Conference on Pattern Recognition,
        art. no. 6976891, pp. 996-1001. 
    """  
    
    def __init__(self, markers='auto', compactness=0.0, radius=2):
        """
        Parameters
        ----------
        markers : Image or str
            'auto' -> Markers are computed automatically on the gradient image
                      by thresholding this at the median level.
            Image  -> An array marking the basins with the values to be assigned 
                      in the label matrix. Zero means not a marker. 
                      Needs to have the same size as the image to segment.
        compactness : float
            Use compact watershed [3] with given compactness parameter. Higher
            values result in more regularly-shaped watershed basins.
        radius : int
            Radius of the structuring element (disc) used to compute the 
            gradient.
        """   
        self.set_markers(markers)
        self._compactness = compactness
        self._radius = radius
        a = 0
        
    def _segment(self, img):
        
        #Compute the gradient image
        gradient_img = rank.gradient(img.get_data(), disk(self._radius))
        
        #Extract the markers
        if self._markers == 'auto':
            markers = gradient_img < np.median(gradient_img)
            markers = ndi.label(markers)[0] 
        else:
            if not (self._markers.get_size() == img.get_size()):
                raise Exception('The markers and the input image must have the'
                                'same size')            
            markers = self._markers.get_data()        
                        
        segmented = watershed(gradient_img, 
                              markers = markers, 
                              connectivity = 1, offset = None, mask = None, 
                              compactness = self._compactness, 
                              watershed_line = False)
        
        return segmented
        
    def set_markers(self, markers):
        """Set the markers
        
        Parameters
        ----------
        markers : Image
            An array marking the basins with the values to be assigned in the 
            label matrix. Zero means not a marker. Need to have the same
            size as the image to segment.
        """
        self._markers = markers
        
    def __repr__(self):
        return self.__class__.__name__ +\
               ' compactness = {:4.2f}'.format(self._compactness) +\
               ' radius = {:4.2f}'.format(self._radius)
 
class Felzenszwalb(ImageSegmenterGS):
    """ Felsenszwalb’s graph-based image segmentation. 
    
    References
    ----------
    [1] Felzenszwalb, P.F., Huttenlocher, D.P. Efficient graph-based image 
    segmentation (2004) International Journal of Computer Vision, 59 (2), 
    pp. 167-181. 
    """
    
    
    def __init__(self, scale = 1.0, sigma = 0.8, min_size = 5):
        """
        Parameters
        ----------
        scale : float
            Free parameter: higher values mean larger clusters.
        sigma : float
            Width (standard deviation) of the Gaussian kernel used for
            image preprocessing.
        min_size : int
            Minimum size of the smallest region. Enforced using postprocessing.
        """ 
        self._scale = scale
        self._sigma = sigma
        self._min_size = min_size
    
    def _segment(self, img):
        segmented = felzenszwalb(image = img.get_data(), 
                                 scale = self._scale, 
                                 sigma = self._sigma,
                                 min_size = self._min_size)
        return segmented
    
    def __repr__(self):
        return self.__class__.__name__ +\
               ' scale = {:4.2f}, sigma = {:4.2f}, min size = {}'.\
               format(self._scale, self._sigma, self._min_size)    
            
class Slic(ImageSegmenter):
    """ Simple Linear Iterative Clustering (SLIC). 
    
    References
    ----------
    [1] Achanta, R., Shaji, A., Smith, K., Lucchi, A., Fua, P., Süsstrunk, S.
        SLIC superpixels compared to state-of-the-art superpixel methods
        (2012) IEEE Transactions on Pattern Analysis and Machine Intelligence,
        34 (11), art. no. 6205760, pp. 2274-2281. 
    """ 
    
    def _check_type(self, img):
        """The input image needs to be RGB or greyscale. 
               
        Parameters
        ----------
        img : Image
            The image to segment
            
        Returns
        -------
        check_ok : bool
            True if img is the right type, false otherwise.
        """
        check_ok = True
        if img.get_type() not in [ImageType.RGB, ImageType.GS]:
            check_ok = False
        
        return check_ok
    
    def __init__(self, n_segments=10, compactness=1.0):
        """
        Parameters
        ----------
        n_segments : int
            The (approximate) number of labels in the segmented output image.
        compactness : float
            Balance between color/greyscale proximity and space proximity. 
            Higher values give more weight to space proximity, making 
            superpixel shapes more square/cubic. 
        """
        self._n_segments = n_segments
        self._compactness = compactness
    
    @doc_inherit
    def _segment(self, img):           
        segmented = slic(img.get_data(), n_segments = self._n_segments, 
                         compactness = self._compactness)
        return segmented

    def __repr__(self):
        return self.__class__.__name__ +\
               ': num segments = {}, compactness = {:4.2f}'.\
               format(self._n_segments, self._compactness)     
    